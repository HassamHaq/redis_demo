﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Redis_Console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Redis redis = new Redis();
            
            #region SingleEntry
            //Set Key Value Pair
            //await redis.SetString("Foo", "Bar");


            //Read Key Value Pair
            //var Data = await redis.GetString("Foo");
            //Console.WriteLine(Data);
            #endregion
            #region ServerDetails
            //Get Server Details
            //var Info = await redis.GetInfo();
            #endregion
            #region MultipleEntries
            await redis.InsertMultiple();
            #endregion
        }

        public class Redis
        {
            private readonly ConnectionMultiplexer _muxer;
            private readonly IDatabase _conn;
            private readonly IServer _server;
            string ipadress = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();

            public Redis()
            {
                _muxer = ConnectionMultiplexer.Connect($"{ipadress}:3000,user=default,password=SuperSecretSecureStrongPass");
                _conn = _muxer.GetDatabase();
                _server = _muxer.GetServer($"{ipadress}:3000");
            }
            
            public async Task SetString(string key, string value)
            {
                await _conn.StringSetAsync(key,value);
             //   _conn.StringSetAsync(
            }

            public async Task<string> GetString(string key)
            {
                return await _conn.StringGetAsync(key);
            }
            
            public async Task<dynamic> GetInfo()
            {
                return await _server.InfoAsync();
            }

            public async Task<bool> InsertMultiple()
            {
                List<KeyValuePair<RedisKey, RedisValue>> _list = new List<KeyValuePair<RedisKey, RedisValue>>();
                for (int i = 0; i < 10; i++)
                {
                    _list.Add(new KeyValuePair<RedisKey, RedisValue>(Guid.NewGuid().ToString(), Guid.NewGuid().ToString()));
                }
                var _array = _list.ToArray();
                When when = When.Always;
                return await _conn.StringSetAsync(_array, when);

            }
        }
    }
}
